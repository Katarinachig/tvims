from numpy import random
from collections import Counter
import matplotlib.pyplot as plt
from prettytable import PrettyTable
import math

a = -7
b = 11


def remake(eps):
    return eps * (b - a) + a


def make_y(x):
    return math.atan(x)


def build_graphics(function, color):
    keys = list(function.keys())
    length = len(keys)
    i = 0
    while i + 1 < length:
        X = [float(keys[i]), float(keys[i + 1])]
        Y = [function[keys[i+1]], function[keys[i+1]]]
        plt.plot(X, Y, color=color)
        i += 1
    X = [float(keys[0]-0.07), float(keys[0])]
    Y = [0, 0]
    plt.plot(X, Y, color=color)
    X = [float(keys[-1]), float(keys[-1] + 0.07)]
    Y = [1, 1]
    plt.plot(X, Y, color=color)


n = int(input("enter n"))
eps = [random.uniform(0, 1) for i in range(n)]
x = map(remake, eps)
y = list(map(make_y, x))
y = sorted(y)
print("simple variation range")
print(y)
var_range = dict(Counter(y))
weighted_table = PrettyTable()
weighted_table.field_names = ["value", "quantity"]
for i in var_range:
    weighted_table.add_row([i, var_range[i]])
print("weighted variation range")
print(weighted_table)
empirical_table = PrettyTable()
empirical_table.field_names = ["value", "frequencies"]
empirical_function = {i: 0 for i in var_range.keys()}
for i in empirical_function.keys():
    for j in var_range.keys():
        if j < i:
            empirical_function[i] += var_range[j]
    empirical_function[i] /= n
    empirical_table.add_row([i, empirical_function[i]])
print("empirical function is")
print(empirical_table)
plt.text(-1.5, 1, "theoretical", color="red", fontsize=14)
plt.text(-1.5, 0.8, "empirical", color="blue", fontsize=14)
print("theoretical function is")
theoretical_table = PrettyTable()
theoretical_table.field_names = ["value", "probability"]
for i in var_range.keys():
    print(i)
    print(math.tan(i)/18 + 7/18)
    theoretical_table.add_row([i, math.tan(i)/18 + 7/18])
i = math.atan(-7)-0.01
x_list = []
y_list = []
while i <= math.atan(11)+0.01:
    x_list.append(i)
    y_list.append(math.tan(i) / 18 + 7 / 18)
    i += 0.01
plt.plot(x_list, y_list,  color="red")
build_graphics(empirical_function, "blue")
print(theoretical_table)
plt.grid()
plt.show()
