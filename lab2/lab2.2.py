from numpy import random
from collections import Counter
import matplotlib.pyplot as plt
from prettytable import PrettyTable
import math

a = -7
b = 11


def M_make(n):
    if n <= 100:
        return int(math.sqrt(n))
    else:
        if n % int(2*math.log10(n)) == 0:
            return int(2*math.log10(n))
        elif n % int(3*math.log10(n)) == 0:
            return int(3*math.log10(n))
        else:
            return int(4 * math.log10(n))


def remake(eps):
    return eps * (b - a) + a


def build_graphics(f, x, color):
    length = len(f)
    i = 0
    while i + 1 < length:
        Y = [f[i], f[i+1]]
        X = [x[i], x[i+1]]
        plt.plot(X, Y, color=color)
        i += 1


def make_y(x):
    return math.atan(x)


def equal_interval_method(y, var_range):
    length = len(y)
    M = M_make(length)
    hi = (y[-1]-y[0])/M
    A = [y[0] + (i-1)*hi for i in range(1, M + 1)]
    B = [A[i + 1] for i in range(M - 1)]
    B.append(y[-1])
    v = [0]*M
    counter = 0
    for i in range(M):
        for j in range(counter, length):
            if y[j] == B[i]:
                if i != M - 1:
                    v[i] += var_range[y[j]] * 0.5
                    v[i + 1] = var_range[y[j]] * 0.5
                else:
                    v[i] += var_range[y[j]]
                counter = j + 1
                break
            else:
                if y[j] < B[i]:
                    v[i] += var_range[y[j]]
                else:
                    counter = j
                    break
    f = [v[i]/(hi * length) for i in range(M)]
    table = PrettyTable()
    table.field_names = ["interval", "f"]
    theoretical_table = PrettyTable()
    theoretical_table.field_names = ["value", "f"]
    for i in range(len(f)):
        table.add_row([str(A[i]) + ";" + str(B[i]), f[i]])
        theoretical_table.add_row([str((A[i] + B[i]) / 2), 1 / (18 * (math.cos((A[i] + B[i]) / 2) ** 2))])
    print(" interval table")
    print(table)
    print("theoretical table")
    print(theoretical_table)
    return f, hi


n = int(input("enter n"))
eps = [random.uniform(0, 1) for i in range(n)]
x = map(remake, eps)
y = list(map(make_y, x))
y = sorted(y)
var_range = dict(Counter(y))
f, h = equal_interval_method(y, var_range)
x = [y[0]+h/2]
for i in range(len(f)-1):
    x.append(x[i] + h)
plt.bar(x, f, h)
build_graphics(f, x, "red")
i = -1.3405
x_list = []
y_list = []
while i <= 1.4:
    x_list.append(i)
    y_list.append(1/(18 * (math.cos(i))**2))
    i += 0.01
plt.plot(x_list, y_list,  color="black")
plt.text(-1.42, 1.54, "theoretical", color="black", fontsize=14)
plt.text(-1.42, 1.45, "polygon", color="red", fontsize=14)
plt.show()
