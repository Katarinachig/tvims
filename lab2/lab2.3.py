from numpy import random
from collections import Counter
import matplotlib.pyplot as plt
from prettytable import PrettyTable
import math


a = -7
b = 11


def M_make(n):
    if n <= 100:
        return int(math.sqrt(n))
    else:
        if n % int(2*math.log10(n)) == 0:
            return int(2*math.log10(n))
        elif n % int(3*math.log10(n)) == 0:
            return int(3*math.log10(n))
        else:
            return int(4 * math.log10(n))


def build_graphics(f, x, color):
    length = len(f)
    i = 0
    while i + 1 < length:
        Y = [f[i], f[i+1]]
        X = [x[i], x[i+1]]
        plt.plot(X, Y, color=color)
        i += 1


def remake(eps):
    return eps * (b - a) + a


def make_y(x):
    return math.atan(x)


def equiprobable_method(y):
    length = len(y)
    M = M_make(length)
    vi = int(length/M)
    A = list()
    A.append(y[0])
    for i in range(1, M):
        A.append((y[i*vi + 1] + y[i * vi]) / 2)
    B = [A[i + 1] for i in range(M - 1)]
    B.append(y[length - 1])
    h = [B[i] - A[i] for i in range(M)]
    f = [vi / (h[i] * length) for i in range(M)]
    table = PrettyTable()
    table.field_names = ["interval", "f"]
    theoretical_table = PrettyTable()
    theoretical_table.field_names = ["value", "f"]
    for i in range(len(f)):
        table.add_row([str(A[i]) + ";" + str(B[i]), f[i]])
        theoretical_table.add_row([str((A[i] + B[i]) / 2), 1 / (18 * (math.cos((A[i] + B[i]) / 2) ** 2))])
    print("table")
    print(table)
    print("theoretical table")
    print(theoretical_table)
    return f, h,


n = int(input("enter n"))
eps = [random.uniform(0, 1) for i in range(n)]
x = map(remake, eps)
x = list(x)
y = list(map(make_y, x))
y = sorted(y)
var_range = dict(Counter(y))
f, h = equiprobable_method(y)
x = [y[0]+(h[0]/2)]
for i in range(len(f)-1):
    x.append(x[i] + h[i]/2 + h[i+1]/2)
plt.bar(x, f, h)
build_graphics(f, x, "red")
i = math.atan(-7)
x_list = []
y_list = []
while i <= math.atan(11):
    x_list.append(i)
    y_list.append(1/(18 * (math.cos(i))**2))
    i += 0.01
plt.plot(x_list, y_list,  color="black")
plt.text(-0.5, 4.6, "theoretical", color="black", fontsize=14)
plt.text(-0.5, 4.3, "polygon", color="red", fontsize=14)
plt.show()
