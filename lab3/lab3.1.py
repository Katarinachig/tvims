from numpy import random
from collections import Counter
import matplotlib.pyplot as plt
from prettytable import PrettyTable
from scipy.stats import chi2
import math


a = -7
b = 11


def M_make(n):
    if n <= 400:
        return int(math.sqrt(n))
    else:
        if n % int(2*math.log10(n)) == 0:
            return int(2*math.log10(n))
        elif n % int(3*math.log10(n)) == 0:
            return int(3*math.log10(n))
        else:
            return int(4 * math.log10(n))


def density_make(a, b, M):
    return 1/((b - a) *M)


def remake(eps):
    return eps * (b - a) + a


def make_y(x):
    return math.atan(x)


def equiprobable_method(y):
    length = len(y)
    M = M_make(length)
    vi = int(length / M)
    A = list()
    A.append(y[0])
    for i in range(1, M):
        A.append((y[i * vi + 1] + y[i * vi]) / 2)
    B = [A[i + 1] for i in range(M - 1)]
    B.append(y[length - 1])
    h = [B[i] - A[i] for i in range(M)]
    f = [vi / (h[i] * length) for i in range(M)]
    table = PrettyTable()
    table.field_names = ["interval", "f"]
    theoretical_table = PrettyTable()
    theoretical_table.field_names = ["value", "f"]
    for i in range(len(f)):
        table.add_row([str(A[i]) + ";" + str(B[i]), f[i]])
        theoretical_table.add_row([str((A[i] + B[i]) / 2), 1 / (18 * (math.cos((A[i] + B[i]) / 2) ** 2))])
    print("table")
    print(table)
    print("theoretical table")
    print(theoretical_table)
    return f, h, M, A, B, vi


def alpha_count(M, A, B, v, n):
    p = [(math.tan(B[i])/18 - math.tan(A[i])/18) for i in range(M)]
    if abs(sum(p) - 1) <= 0.05:
        alpha_list = [((v-n*p[i])**2)/(n*p[i]) for i in range(M)]
        alpha_in_square = sum(alpha_list)
        k = M - 1
        a = float(input("enter alpha"))
        alpha = chi2.isf(a, k)
        if alpha_in_square > alpha:
            print("false")
        else:
            print("true")
    else:
        print("incorrect p")


n = int(input("enter n"))
eps = [random.uniform(0, 1) for i in range(n)]
x = map(remake, eps)
y = list(map(make_y, x))
y = sorted(y)
var_range = dict(Counter(y))
sorted(var_range)
f, h, M, A, B, v = equiprobable_method(y)
alpha_count(M, A, B, v, n)
x = [y[0]+(h[0]/2)]
for i in range(len(f)-1):
    x.append(x[i] + h[i]/2 + h[i+1]/2)
plt.bar(x, f, h)
i = -1.42
x_list = []
y_list = []
while i <= 1.47:
    x_list.append(i)
    y_list.append(1/(18 * (math.cos(i))**2))
    i += 0.01
plt.plot(x_list, y_list,  color="black")
plt.show()