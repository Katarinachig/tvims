from numpy import random
from collections import Counter
import matplotlib.pyplot as plt
import math
from scipy.stats import ksone


a = -7
b = 11
a_theoretical = -1.5
b_theoretical = 1.5


def remake(eps):
    return eps * (b - a) + a


def make_y(x):
    return math.atan(x)


def build_graphics(function, color):
    keys = list(function.keys())
    length = len(keys)
    i = 0
    while i + 1 < length:
        X = [float(keys[i]), float(keys[i + 1])]
        Y = [function[keys[i+1]], function[keys[i+1]]]
        plt.plot(X, Y, color=color)
        i += 1
    X = [float(keys[0]-0.07), float(keys[0])]
    Y = [0, 0]
    plt.plot(X, Y, color=color)
    X = [float(keys[-1]), float(keys[-1] + 0.07)]
    Y = [1, 1]
    plt.plot(X, Y, color=color)


n = int(input("enter n"))
eps = [random.uniform(0, 1) for i in range(n)]
x = map(remake, eps)
x = sorted(x)
y = map(make_y, x)
y = sorted(y)
var_range = dict(Counter(y))
empirical_function = {i: 0 for i in var_range.keys()}
for i in empirical_function.keys():
    for j in var_range.keys():
        if j < i:
            empirical_function[i] += var_range[j]
    empirical_function[i] /= n
plt.text(-1.5, 1, "theoretical", color="red", fontsize=14)
plt.text(-1.5, 0.8, "empirical", color="blue", fontsize=14)
i = -1.43
x_list = []
y_list = []
while i <= 1.4802:
    x_list.append(i)
    y_list.append(math.tan(i) / 18 + 7 / 18)
    i += 0.01
plt.plot(x_list, y_list,  color="red")
values = list(var_range.keys())
max_delta = max([max([abs((i + 1)/n - (math.tan(values[i])/18 + 7/18)), abs(math.tan(values[i])/18 + 7/18 - ((i + 1-1)/n))]) for i in range(n)])
alpha = math.sqrt(n) * max_delta
a = float(input("enter alpha"))
alpha_k = n ** 0.5 * ksone.ppf(1 - (1-a)/2, n)
if alpha > alpha_k:
    print("false")
else:
    print("true")
build_graphics(empirical_function, "blue")
plt.grid()
plt.show()
