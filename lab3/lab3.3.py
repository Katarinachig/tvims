from numpy import random
from collections import Counter
import math
from scipy.stats import vonmises

a = -7
b = 11
a_theoretical = -1.5
b_theoretical = 1.5


def remake(eps):
    return eps * (b - a) + a


def make_y(x):
    return math.atan(x)


n = int(input("enter n"))
eps = [random.uniform(0, 1) for i in range(n)]
x = map(remake, eps)
x = sorted(x)
y = map(make_y, x)
y = sorted(y)
var_range = dict(Counter(y))
sorted(var_range)
values = list(var_range.keys())
statistic_criteria = 1/(12*n)*sum([(math.tan(values[i])/18 + 7/18 - (i + 1-0.5)/n)**2 for i in range(n)])
a = float(input("enter alpha"))
delta = vonmises.ppf(1 - a/2, n)
if delta > statistic_criteria:
    print("true")
else:
    print("false")
