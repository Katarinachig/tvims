import random
import matplotlib.pyplot as plt
from scipy import integrate
from scipy.stats import t
from scipy.special import erf
import math

a = -7
b = 11
a_theoretical = -1.5
b_theoretical = 1.5
interval_map = {0.005 + 0.005 * i: () for i in range(0, 198)}
n_list = {20: "red", 30: "green", 50 : "black", 70: "yellow", 100: "blue", 150: "purple"}

def remake(eps):
    return eps * (b - a) + a


def make_y(x):
    return math.atan(x)


def dotted_mathematical_waiting(x, n):
    return sum(x)/n


def dotted_dispersion(x, n, mx):
    temp_list = [(x[i] - mx)**2 for i in range(n)]
    return (1/(n-1)) * sum(temp_list)


def interval_mathematical_expectation(m, d):
    for i in interval_map.keys():
        st = t.ppf(i / 2, n-1) # или 1-i/2???
        interval_map[i] = (m - d * st / math.sqrt(n), m + d * st / math.sqrt(n)) # или корень таки из n - 1??


def build_graphics(interval_map, color):
    keys = list(interval_map.keys())
    for key in keys:
        X = [float(key), float(key)]
        Y = [interval_map[key][0], interval_map[key][1]]
        plt.plot(X, Y, color=color)


def theoretical_math_waiting():
    result = integrate.quad(lambda x: x * 1/(18 * (math.cos(x)**2)), math.atan(-7), math.atan(11))
    return result[0]


def theoretical_dispersion(math_waiting):
    square_math_waiting = integrate.quad(lambda x: (x * x)/(18 * (math.cos(x)**2)), math.atan(-7), math.atan(11))
    return square_math_waiting[0] - math_waiting ** 2


def interval_mathematical_expectation_with_D(m, D):
    laplace = lambda x: erf(x / 2 ** 0.5)
    for i in interval_map.keys():
        lap = laplace((1 - i)/2)
        interval_map[i] = (m - math.sqrt(D / n) * lap, m + math.sqrt(D / n) * lap)


def make_list(n):
    eps = [random.uniform(0, 1) for i in range(n)]
    x = map(remake, eps)
    y = list(map(make_y, x))
    y = sorted(y)
    return y


def find_dotted_dispersion(y, n):
    mx = dotted_mathematical_waiting(y, n)
    return dotted_dispersion(y, n, mx)


m = theoretical_math_waiting()
d = theoretical_dispersion(m)
for n in n_list.keys():
    y = make_list(n)
    m = dotted_mathematical_waiting(y, n)
    interval_mathematical_expectation_with_D(m, d)
    build_graphics(interval_map, n_list[n])
plt.show()
