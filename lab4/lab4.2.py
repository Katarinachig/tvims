from numpy import random
import matplotlib.pyplot as plt
from scipy import integrate
from scipy.stats import chi2
import math

a = -7
b = 11
a_theoretical = -1.5
b_theoretical = 1.5
interval_map = {0.005 + 0.005 * i: () for i in range(0, 198)}


def remake(eps):
    return eps * (b - a) + a


def make_y(x):
    return math.atan(x)


def dotted_mathematical_waiting(x, n):
    return sum(x)/n


def dotted_dispersion(x, n, mx):
    temp_list = [(x[i] - mx)**2 for i in range(n)]
    return (1/(n-1)) * sum(temp_list)


def build_graphics(interval_map, color):
    keys = list(interval_map.keys())
    for key in keys:
        X = [float(key), float(key)]
        Y = [interval_map[key][0], interval_map[key][1]]
        plt.plot(X, Y, color=color)


def interval_dispersion(d, n):
    for i in interval_map.keys():
        alpha_a = chi2.isf(float(i)/2, n - 1)
        alpha_b = chi2.isf(1 - float(i)/2, n - 1)
        interval_map[i] = ((n-1) * d/alpha_a, (n-1) * d/alpha_b)
    return interval_map


def theoretical_math_waiting():
    result = integrate.quad(lambda x: x * 1/(18 * (math.cos(x)**2)), math.atan(-7), math.atan(11))
    return result[0]


def theoretical_dispersion(math_waiting):
    square_math_waiting = integrate.quad(lambda x: x * x * 1/(18 * (math.cos(x)**2)), math.atan(-7), math.atan(11))
    return square_math_waiting[0] - math_waiting ** 2


def theoretical_interval_dispersion_for_other(m, y, n):
    S = (sum([(i - m) ** 2 for i in y]))/n
    global interval_map
    for i in interval_map.keys():
        alpha_a = chi2.isf((float(i)) / 2, n)
        alpha_b = chi2.isf((1 - float(i)) / 2, n)
        interval_map[i] = (n * S/alpha_a, n * S/alpha_b)


def make_list(n):
    eps = [random.uniform(0, 1) for i in range(n)]
    x = map(remake, eps)
    y = list(map(make_y, x))
    y = sorted(y)
    return y


def find_dotted_dispersion(y, n):
    mx = dotted_mathematical_waiting(y, n)
    return dotted_dispersion(y, n, mx)


n = 20
y = make_list(n)
mx = dotted_mathematical_waiting(y, n)
print("dotted mathematical waiting = " + str(mx))
d = dotted_dispersion(y, n, mx)
print("dotted dotted dispersion = " + str(d))
plt.text(0, 3.7, "interval for dispersion for 20", color="red", fontsize=14)
plt.text(0, 3.5, "for 30", color="green", fontsize=14)
plt.text(0.15, 3.5, "for 50", color="black", fontsize=14)
plt.text(0.3, 3.5, "for 70", color="yellow", fontsize=14)
plt.text(0, 3.3, "for 100", color="blue", fontsize=14)
plt.text(0.15, 3.3, "for 150", color="purple", fontsize=14)
interval_dispersion(d, 20)
build_graphics(interval_map, "red")
n = 30
y = make_list(n)
d = find_dotted_dispersion(y, n)
interval_dispersion(d, n)
build_graphics(interval_map, "green")
n = 50
y = make_list(n)
d = find_dotted_dispersion(y, n)
interval_dispersion(d, n)
build_graphics(interval_map, "black")
n = 70
y = make_list(n)
d = find_dotted_dispersion(y, n)
interval_dispersion(d, n)
build_graphics(interval_map, "yellow")
n = 100
y = make_list(n)
d = find_dotted_dispersion(y, n)
interval_dispersion(d, n)
build_graphics(interval_map, "blue")
n = 150
y = make_list(n)
d = find_dotted_dispersion(y, n)
interval_dispersion(d, n)
build_graphics(interval_map, "purple")
plt.show()
plt.text(0, 4, "interval for dispersion for 20", color="red", fontsize=14)
plt.text(0, 4.2, "interval for dispersion for 20 with known mx", color="blue", fontsize=14)
n = 20
y = make_list(n)
mx = theoretical_math_waiting()
theoretical_interval_dispersion_for_other(mx, y, 20)
build_graphics(interval_map, "blue")
#plt.show()
d = find_dotted_dispersion(y, n)
interval_dispersion(d, 20)
build_graphics(interval_map, "red")
plt.show()
plt.text(0, 3.7, "interval for dispersion for 20 with known mx", color="red", fontsize=14)
plt.text(0, 3.5, "for 30", color="green", fontsize=14)
plt.text(0.3, 3.5, "for 50", color="black", fontsize=14)
plt.text(0.15, 3.5, "for 70", color="yellow", fontsize=14)
plt.text(0, 3.3, "for 100", color="blue", fontsize=14)
plt.text(0.3, 3.3, "for 150", color="purple", fontsize=14)
theoretical_interval_dispersion_for_other(mx, y, 20)
build_graphics(interval_map, "red")
y = make_list(30)
theoretical_interval_dispersion_for_other(mx, y, 30)
build_graphics(interval_map, "green")
y = make_list(50)
theoretical_interval_dispersion_for_other(mx, y, 50)
build_graphics(interval_map, "black")
y = make_list(70)
theoretical_interval_dispersion_for_other(mx, y, 70)
build_graphics(interval_map, "yellow")
y = make_list(100)
theoretical_interval_dispersion_for_other(mx, y, 100)
build_graphics(interval_map, "blue")
y = make_list(150)
theoretical_interval_dispersion_for_other(mx, y, 150)
build_graphics(interval_map, "purple")
plt.show()
